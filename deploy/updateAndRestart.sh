#!/bin/bash

# any future command that fails will exit the script
set -e

# Delete the old repo
rm -rf /home/ubuntu/test_project/

# clone the repo again
git clone https://gitlab.com/Suraj_Hegde123/test_project.git
#source the nvm file. In an non
#If you are not using nvm, add the actual path like
source /home/ubuntu/.nvm/nvm.sh
nvm install node
# stop the previous pm2
pm2 kill
npm remove pm2 -g


#pm2 needs to be installed globally as we would be deleting the repo folder.
# this needs to be done only once as a setup script.
npm install pm2 -g
# starting pm2 daemon
pm2 status

cd /home/ubuntu/test_project

echo "Install npm and nodejs"
sudo apt install node.js
#install npm packages
echo "Running npm install"
npm install

#Restart the node server
npm start
